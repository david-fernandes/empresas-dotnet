﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class EnterpriseType
    {
        public int id { get; set; }
        public string enterprise_type_name { get; set; }
    }
}