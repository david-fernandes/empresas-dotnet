﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Test.Models;

namespace Test.Controllers
{
    public class EnterprisesController : ApiController
    {
        private EnterprisesContext db = new EnterprisesContext();

        //GET
        [Authorize]
        [Route("api/v1/enterprises")]
        public IQueryable<Enterprise> GetEnterprises()
        {
            var enterprises = db.Enterprises.Include(x => x.enterprise_type);
            return enterprises;
        }

        //GET
        [Authorize]
        [Route("api/v1/enterprises")]
        public IQueryable<Enterprise> GetEnterprises([FromUri] int enterprise_types, [FromUri] string name)
        {
            var enterprise = db.Enterprises.Include(x => x.enterprise_type);
            enterprise = (from c in enterprise where c.enterprise_type.id == enterprise_types && c.enterprise_name.Contains(name) select c);

            return enterprise;
        }

        //GET
        [Authorize]
        [Route("api/v1/enterprises/{id:int}")]
        [ResponseType(typeof(Enterprise))]
        public IHttpActionResult GetEnterprise(int id)
        {
            var enterprise = db.Enterprises.Include(x => x.enterprise_type);
            enterprise = (from c in enterprise where c.id == id select c);

            if (enterprise == null)
            {
                return NotFound();
            }

            return Ok(enterprise);
        }

        //POST
        [Authorize]
        [Route("api/v1/enterprises")]
        [ResponseType(typeof(Enterprise))]
        public IHttpActionResult PostEnterprise(Enterprise enterprise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Enterprises.Add(enterprise);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = enterprise.id }, enterprise);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}